const fs = require('fs')
const Cron = require("node-cron");
const spawn = require('child_process').spawn
const AWS = require('aws-sdk');  
const env = require('dotenv');
env.config();


const ID_AWS = process.env.ID_AWS;
const SECRET_AWS = process.env.SECRET_AWS;
const BUCKET_NAME_AWS = process.env.BUCKET_NAME_AWS;
const s3 = new AWS.S3({
  accessKeyId: ID_AWS,
  secretAccessKey: SECRET_AWS
});

const uploadFile = (fileName) => {
  const fileContent = fs.readFileSync(fileName);
  const params = {
      Bucket: BUCKET_NAME_AWS,
      Key: fileName,
      Body: fileContent,
      ACL:'private'
  };
  s3.upload(params, function(err, data) {
      if (err) {
          throw err;
      }
      console.log(`File uploaded successfully. ${data.Location}`);
  });
};

Cron.schedule("* * * * * ", async () => {     //run every minute
    const dumpFileName = `Rifki_db_of ${Math.round(Date.now() / 1000)}.dump.sql` //sebaiknya pake feature time agar waktu bisa beragam wib, wit, wita.
    const writeStream = fs.createWriteStream(dumpFileName)
    const dump = spawn('mysqldump', [
        '-P',
        `${process.env.PORT}`,
        '-h',
        `${process.env.HOST}`,
        '-u',
        `${process.env.USERNAME}`,
        `${process.env.PASSWORD}`,
        `${process.env.DATABASE}`,
    ])
    await dump
    .stdout
    .pipe(writeStream)
    .on('finish', async function () {
        await uploadFile(dumpFileName)
        console.log('Dump Completed')
    })
    .on('error', function (err) {
        console.log(err)
    })
    
  });
  
  Cron.schedule("* * * * * ", () => {                 //run every minute
    console.log("CRON Completed in every minute");
  });
